/*const http=require('http');
const port=3002;
const server=http.createServer();

server.on('request', (request, response) => {
		console.log(`URL is : ${request.url}`);
		response.log(100)
		response.end('Hello,server')
	}
	)

server.listen(
	port, (error) => 
	{
		if (error) return console.log(`Error: $(error)`);
		console.log(`Server is listening on port: ${port}`)
	}
	)
*/
var express = require('express');
var app = express();

app.get('/', function (req, res) {
   res.send('Hello World');
})

var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port
   
   console.log("Example app listening at http://%s:%s", host, port)
})
